import { useRef } from 'react'
import './index.scss'

const Logo = () => {
  const bgRef = useRef()
  const animationTitle = 'Joe Web Dev'

  return (
    <div className="logo-container" ref={bgRef}>
      <h2 class="jt --debug">
        <span class="jt__row">
          <span class="jt__text">{animationTitle}</span>
        </span>
        <span class="jt__row jt__row--sibling" aria-hidden="true">
          <span class="jt__text">{animationTitle}</span>
        </span>
        <span class="jt__row jt__row--sibling" aria-hidden="true">
          <span class="jt__text">{animationTitle}</span>
        </span>
        <span class="jt__row jt__row--sibling" aria-hidden="true">
          <span class="jt__text">{animationTitle}</span>
        </span>
      </h2>
    </div>
  )
}

export default Logo
